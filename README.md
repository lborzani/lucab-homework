# Users Api

This project contains the Users Api [specification](./openapi.yaml) and implementation.

The Api provides basic operations to:

* create a new user
* search a user by email or firstName
* update a user (full or partial)


# Getting Started

The project can be built with maven, use the provided wrapper `mvnw clean package`.
Note that the `JAVA_HOME` variable must be set in your environment for the wrapper to work.

Before you can run the application you need to get a running kafka cluster.

## Getting Kafka

The application will fail to start because it expects to connect to a kafka broker.
The simplest way to get a single node kafka cluster running locally is to use the provided [docker-compose](single-node-kafka.yaml)

To run it, type in a terminal `docker-compose -f singe-node-kafka.yaml up -d` (be sure to be in the same folder as the docker-compose file).

Note: you must have Docker installed on your machine, refer to online documentation on how to install it
or download the standalone kafka cluster following these [instructions](https://docs.confluent.io/current/quickstart/ce-quickstart.html#ce-quickstart).

If you are using the provided docker-compose file you should be able to verify kafka is running with `docker ps`,
you should get something like this:

```
docker ps
CONTAINER ID        IMAGE                         COMMAND                  CREATED             STATUS              PORTS                                              NAMES
b53055ae3955        confluentinc/cp-kafka:5.5.1   "/etc/confluent/dock…"   11 seconds ago      Up 10 seconds       0.0.0.0:9092->9092/tcp, 0.0.0.0:19092->19092/tcp   kafka
3eaba7d069bc        zookeeper:3.4.9               "/docker-entrypoint.…"   11 seconds ago      Up 10 seconds       2888/tcp, 0.0.0.0:2181->2181/tcp, 3888/tcp         zookeeper
```

Note: if you don't see the kafka container, it's possible that kafka failed to start because zookeeper was not available yet,
just re-run the docker-compose command and it should get fixed.

## Running the application

Now you can run the project with `mvn spring-boot:run` (or use the wrapper).

If the application starts correctly you will see something like this:

```text
(output truncated)
2020-10-06 13:33:38.796  INFO 18796 --- [           main] o.a.kafka.common.utils.AppInfoParser     : Kafka version: 2.5.1
2020-10-06 13:33:38.796  INFO 18796 --- [           main] o.a.kafka.common.utils.AppInfoParser     : Kafka commitId: 0efa8fb0f4c73d92
2020-10-06 13:33:38.797  INFO 18796 --- [           main] o.a.kafka.common.utils.AppInfoParser     : Kafka startTimeMs: 1601984018795
2020-10-06 13:33:39.090  INFO 18796 --- [           main] ch.ricardo.homework.HomeworkApplication  : Started HomeworkApplication in 3.076 seconds (JVM running for 3.391)
```

The application is running on `localhost:8080` and you can access the api at `localhost:8080/users`.

After you have built the project you can consult the generated api documentation [here](target/generated-sources/openapi/index.html).
You can test the api using a client of your choice.

Note: when testing from localhost change `geolocation.style` in the `application.yaml` to be equal to `mock`.
This will always map your client to one of the whitelisted ips `api.users.country-code-filter.whitelist`.
In any case you can turn off the entire feature by setting `api.users.country-code-filter.enabled=false`.

