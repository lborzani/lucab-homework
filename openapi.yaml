openapi: "3.0.1"
info:
  version: 1.0.0
  title: Users REST API
  description: CRUD operations on Users

servers:
  - description: Dev server
    url: http://localhost:8080

paths:
  /users:
    post:
      summary: Adds a new user to the system
      operationId: create
      requestBody:
        description: The user to create
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/User"
      responses:
        201:
          description: User created
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UserPublic"
        default:
          description: Api error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ErrorResponse"
  /users/{user_id}:
    # partial updates are in theory supported by the PATCH verb
    # but as it is not widely used i will keep PUT for both
    put:
      summary: Updates a user.
      operationId: update
      parameters:
        - name: user_id
          in: path
          required: true
          description: Id of user
          schema:
            type: integer
            format: int64
      requestBody:
        description: ¦
          Updates an existing user. Partial updates are supported. A missing value will result in that field not
          being updated.
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/UserUpdate"
      responses:
        200:
          description: User updated
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UserPublic"
        default:
          description: Api error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ErrorResponse"
  /users/_search:
    post:
      summary: Retrieves one or more users matching the criteria (first name, email)
      operationId: search
      requestBody:
        description: Retrieves one or more users matching the criteria
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/UserCriteria"
      responses:
        200:
          description: The matching users (or empty if nothing matched)
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/UserPublic"
        default:
          description: Api error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ErrorResponse"

components:
  schemas:
    User:
      description: A user of the system
      type: object
      required:
        - email
        - password
        - firstName
        - address
      properties:
        email:
          description: User email
          type: string
          format: email
        password:
          description: User password
          type: string
          format: password
        firstName:
          description: User first name
          type: string
        # shortcut: address as a string field
        address:
          description: User address
          type: string

    UserUpdate:
      description: Version of the user for updates
      type: object
      required:
        - email
        - firstName
        - address
      properties:
        email:
          description: User email
          type: string
          format: email
        firstName:
          description: User first name
          type: string
        address:
          description: User address
          type: string

    UserPublic:
      description: A public view of a user of the system
      type: object
      properties:
        id:
          description: User id
          type: integer
          format: int64
        email:
          description: User email
          type: string
          format: email
        firstName:
          description: User first name
          type: string
        address:
          description: User address
          type: string

    UserCriteria:
      description: ¦
        User search criteria. Supports only Equals matchers.

      type: object
      properties:
        firstName:
          description: Matches users by firstname
          type: string
        email:
          description: Matches users by email
          type: string
          format: email

    ErrorResponse:
      description: RFC7807 compliant error model
      properties:
        type:
          description: A URI reference [RFC3986] that identifies the problem type.
          type: string
          example: about:blank
        title:
          description: A short, human-readable summary of the problem type.
          type: string
          example: Invalid Job Header
        status:
          description: The HTTP status code ([RFC7231], Section 6) generated by the origin server for this occurrence of the problem.
          type: integer
          format: int32
          example: 400
        detail:
          description: A human-readable explanation specific to this occurrence of the problem.
          type: string
          example: Required 'channel-name' metadata is missing.