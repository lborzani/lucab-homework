package ch.ricardo.homework;

import ch.ricardo.homework.openapi.model.User;
import ch.ricardo.homework.openapi.model.UserCriteria;
import ch.ricardo.homework.openapi.model.UserPublic;
import ch.ricardo.homework.openapi.model.UserUpdate;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;

import javax.transaction.Transactional;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// shortcut: i create this one fat test to get a general confirmation that nothing is broken
// because i feel unnecessary to provide a full set of unit tests and proper integration tests
// in the context of this home assignment. I consider enough to hint the fact that tests are mandatory :P
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"geolocation.style=mock"})
@EmbeddedKafka(
        partitions = 1,
        bootstrapServersProperty = "spring.kafka.bootstrap-servers",
        topics = { "users-topic" }
)
@Transactional
class HomeworkApplicationTests {

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    // shortcut: testing the happy path (sanity check)
    @Test
    void bigFatTest() throws Exception {
        final String baseUri = "http://localhost:" + port + "/users";
        User user = new User()
                .firstName("john")
                .email("john.doe@example.com")
                .password("john123")
                .address("Musterstrasse 25, 6321 Dorf");

        // note: api tests may be partially automated with the contract-verifier maven plugin
        // that accepts test specs in yaml format and will generate test-cases using RestAssured
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> httpEntity = new HttpEntity<>(objectMapper.writeValueAsString(user), headers);
        UserPublic createdUser = restTemplate.postForObject(baseUri, httpEntity, UserPublic.class);

        Long userId = createdUser.getId();
        assertNotNull(userId, () -> "It should create a new unique user id");
        assertEquals(user.getFirstName(), createdUser.getFirstName());
        assertEquals(user.getAddress(), createdUser.getAddress());
        assertEquals(user.getEmail(), createdUser.getEmail());

        // let's see if we can find it
        httpEntity = new HttpEntity<>(objectMapper.writeValueAsString(new UserCriteria().email(user.getEmail())), headers);
        UserPublic[] matchingUsers = restTemplate.postForObject(baseUri + "/_search", httpEntity, UserPublic[].class);

        assertEquals(1, matchingUsers.length);
        assertEquals(userId, matchingUsers[0].getId(), () -> "It should match user id " + userId);

        httpEntity = new HttpEntity<>(objectMapper.writeValueAsString(new UserCriteria().firstName("Johnathan")), headers);
        matchingUsers = restTemplate.postForObject(baseUri + "/_search", httpEntity, UserPublic[].class);
        assertEquals(0, matchingUsers.length, () -> "It should not match any user");

        UserUpdate updatingUser = new UserUpdate().firstName("Johnathan");
        httpEntity = new HttpEntity<>(objectMapper.writeValueAsString(updatingUser), headers);
        UserPublic updatedUser = restTemplate.exchange(baseUri + "/{user_id}", HttpMethod.PUT,
                httpEntity, UserPublic.class, Map.of("user_id", userId)).getBody();

        assertEquals(userId, updatedUser.getId(), () -> "It should update user with id " + userId);
        assertEquals(updatedUser.getFirstName(), updatedUser.getFirstName(), () -> "It should update the first name");


        httpEntity = new HttpEntity<>(objectMapper.writeValueAsString(new UserCriteria().firstName("Johnathan")), headers);
        matchingUsers = restTemplate.postForObject(baseUri + "/_search", httpEntity, UserPublic[].class);
        assertEquals(1, matchingUsers.length, () -> "It should match the updated user");
        assertEquals(userId, matchingUsers[0].getId());

    }

}
