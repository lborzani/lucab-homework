package ch.ricardo.homework.repositories;

import ch.ricardo.homework.model.UserEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import java.util.Objects;

@Component
public class UserEntityListener {

    // shortcut: this could be abstracted behind our own "notifier" interface
    // just in case tomorrow we want to switch to another broker.
    private KafkaTemplate kafkaTemplate;
    private String usersTopic;

    public UserEntityListener() {
    }

    @Autowired
    public UserEntityListener(KafkaTemplate kafkaTemplate, @Value("${messaging.users-topic}") String usersTopic) {
        this.kafkaTemplate = Objects.requireNonNull(kafkaTemplate, () -> "A kafka producer is required");
        this.usersTopic = Objects.requireNonNull(usersTopic, () -> "The topic name is required");
    }

    @PostPersist
    @PostUpdate
    public void onUserUpdated(UserEntity entity) {
        // shortcut: there is of course no coordination between the db transaction and kafka transaction
        // a distributed transaction should be used or in alternative block on the send()
        // and in case of a failure rollback the sql tx
        kafkaTemplate.send(usersTopic, UserEvent.of(entity));
        // shortcut2: i assume we don't need a discriminator in the event (for example, is_new: bool)
        // because the consumer side can "easily" aggregate on its side by user.id and event.timestamp
        // and derive the information if the record "is_new" or not
    }
}
