package ch.ricardo.homework.controllers;

import ch.ricardo.homework.exceptions.InvalidSearchCriteria;
import ch.ricardo.homework.openapi.api.UsersApi;
import ch.ricardo.homework.openapi.model.User;
import ch.ricardo.homework.openapi.model.UserCriteria;
import ch.ricardo.homework.openapi.model.UserPublic;
import ch.ricardo.homework.openapi.model.UserUpdate;
import ch.ricardo.homework.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@RestController
public class UsersController implements UsersApi {

    private UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = Objects.requireNonNull(usersService, () -> "UsersService is required");
    }

    @Override
    public ResponseEntity<UserPublic> create(@Valid User user) {
        UserPublic newUser = usersService.create(user);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<UserPublic>> search(@Valid UserCriteria userCriteria) {
        validate(userCriteria);
        List<UserPublic> matches = usersService.search(userCriteria);
        return ResponseEntity.ok(matches);
    }

    @Override
    public ResponseEntity<UserPublic> update(Long userId, @Valid UserUpdate user) {
        UserPublic updatedUser = usersService.update(userId, user);
        return ResponseEntity.ok(updatedUser);
    }

    private void validate(UserCriteria userCriteria) {
        // shortcut: this could be achieved with another aspect or some methodargumentinterceptor
        if (userCriteria.getEmail() == null && userCriteria.getFirstName() == null) {
            throw new InvalidSearchCriteria("You must specify one of email or firstName");
        }
    }
}
