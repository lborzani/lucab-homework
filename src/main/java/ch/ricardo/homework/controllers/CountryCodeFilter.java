package ch.ricardo.homework.controllers;

import ch.ricardo.homework.exceptions.GeolocationException;
import ch.ricardo.homework.services.GeolocationService;
import org.springframework.http.HttpMethod;
import org.springframework.util.Assert;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Ensures the request to POST /users is originated by an ip belonging to a whitelisted country (i.e. CH)
 * <p>
 * shortcut: code in a filter mainly because the openapi generator doesn't easily allow to pass the request
 * to the controller as a parameter (unless the templates are changed)
 * in a real setup i would consider moving this to an api-gateway
 */
public class CountryCodeFilter implements Filter {

    private GeolocationService geolocationService;
    private List<String> whitelist;

    public CountryCodeFilter(GeolocationService geolocationService, List<String> whitelist) {
        this.geolocationService = Objects.requireNonNull(geolocationService, () -> "GeolocationService is required");
        Assert.notEmpty(whitelist, () -> "A whitelist of country codes is required");
        this.whitelist = Collections.unmodifiableList(whitelist);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        if (HttpMethod.POST.matches(req.getMethod())) {
            // shortcut: assuming no proxies/balancers/vpns in between
            String clientIp = req.getRemoteAddr();
            try {
                String countryCode = geolocationService.countryCode(clientIp);
                if (whitelist.contains(countryCode)) {
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    // shortcut: brutal interruption - should return json error response harmonized with the rest
                    res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Usage of the api is not allowed in your country");
                }
            } catch (GeolocationException e) {
                // shortcut: if the geolocation service is unavailable let's return a 500
                // (a circuit breaker could be considered)
                res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
            }
        }
    }
}
