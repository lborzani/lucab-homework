package ch.ricardo.homework.services;

public interface GeolocationService {

    /**
     * Determines the 2 letters country code given the ip
     *
     * @param ip -  ip to geolocate
     * @return the country code
     * @throws ch.ricardo.homework.exceptions.GeolocationException - if the request is invalid or the service is unavailable
     */
    String countryCode(String ip);
}
