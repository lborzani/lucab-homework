package ch.ricardo.homework.services;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@ConditionalOnProperty(value = "geolocation.style", havingValue = "ipapi")
@Component
public class IpapiGeolocationService implements GeolocationService {

    private RestTemplate restTemplate;

    public IpapiGeolocationService(RestTemplate restTemplate) {
        this.restTemplate = Objects.requireNonNull(restTemplate, () -> "RestTemplate is required");
    }

    @Override
    public String countryCode(String ip) {
        try {
            IpapiResponse response = restTemplate.getForObject(
                    String.format("https://ipapi.co/%s/json/", ip), IpapiResponse.class);
            // the api seems to response always with a 200
            // in case of invalid request it will respond with a error=true and a reason.
            if (response.error) {
                throw new RuntimeException("api exception");
            } else {
                return response.countryCode;
            }
        } catch (RestClientException e) {
            throw new RuntimeException("geolocation exception", e);
        }
    }

    // as this is not used anywhere else i keep here for simplicity
    @JsonIgnoreProperties(ignoreUnknown = true)
    class IpapiResponse {
        @JsonProperty("country_code")
        String countryCode;

        String ip;
        boolean error = false;
        String reason;

        public IpapiResponse() {
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public boolean isError() {
            return error;
        }

        public void setError(boolean error) {
            this.error = error;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }
    }
}
