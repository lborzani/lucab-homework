package ch.ricardo.homework.services;

import ch.ricardo.homework.exceptions.UserNotFoundException;
import ch.ricardo.homework.openapi.model.User;
import ch.ricardo.homework.openapi.model.UserCriteria;
import ch.ricardo.homework.openapi.model.UserPublic;
import ch.ricardo.homework.openapi.model.UserUpdate;
import ch.ricardo.homework.repositories.UserEntity;
import ch.ricardo.homework.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class UsersService {

    private UsersRepository repository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UsersService(UsersRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = Objects.requireNonNull(repository, () -> "UsersRepository is required");
        this.passwordEncoder = Objects.requireNonNull(passwordEncoder, () -> "PasswordEncoder is required");
    }

    public UserPublic create(User user) {
        // shortcut: not adding a mapping lib for a few fields
        UserEntity entity = UserEntity.builder()
                .address(user.getAddress())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                // note: this could be achieved in the Entity Listener - possibly cleaner
                .password(passwordEncoder.encode(user.getPassword()))
                .build();
        entity = repository.save(entity);
        // not sending the password back - even if hashed
        // shortcut: for brevity i'm not using json views, but that's a possibility to consider
        return toUserPublic(entity);
    }

    // note: if the search gets more sophisticated it should be moved to another specialized service
    // here we would start mixing too many responsibilities
    public List<UserPublic> search(UserCriteria userCriteria) {
        // shortcut: a simple query by example is enough in this case
        List<UserEntity> entities = repository.findAll(Example.of(UserEntity.builder()
                .firstName(userCriteria.getFirstName())
                .email(userCriteria.getEmail())
                .build()));
        return entities.stream().map(this::toUserPublic).collect(toList());
    }

    public UserPublic update(Long userId, UserUpdate user) {
        // shortcut: if this is a frequent operation i would rather do a "update user set ... where id = ?"
        // instead of this fetch-and-update, i choose this approach because it saves me the time
        // to create a @Modifying query in the repository
        Optional<UserEntity> maybeEntity = repository.findById(userId);
        if (maybeEntity.isPresent()) {
            UserEntity entity = maybeEntity.get();
            entity = updateWithUser(entity, user);
            return toUserPublic(repository.save(entity));
        } else {
            throw new UserNotFoundException("not found");
        }
    }

    /* MapStruct or ModelMapper can probably achieve the same as the following 2 methods */
    /* not used here for the sake of simplicity */

    private UserEntity updateWithUser(UserEntity entity, UserUpdate user) {
        // shortcut: no need for additional mapping libs for this simple case
        if (user.getAddress() != null) {
            entity.setAddress(user.getAddress());
        }
        if (user.getEmail() != null) {
            // email must be unique after update
            entity.setEmail(user.getEmail());
        }
        if (user.getFirstName() != null) {
            entity.setFirstName(user.getFirstName());
        }
        // do not allow password update this way, there should be a dedicated credentials update api for that
        return entity;
    }

    private UserPublic toUserPublic(UserEntity entity) {
        return new UserPublic()
                .id(entity.getId())
                .address(entity.getAddress())
                .firstName(entity.getFirstName())
                .email(entity.getEmail());
    }
}
