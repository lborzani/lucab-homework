package ch.ricardo.homework.services;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;

@ConditionalOnProperty(value = "geolocation.style", havingValue = "mock")
@Component
public class MockGeolocationService implements GeolocationService {

    private String countryCode;

    public MockGeolocationService(@Value("${api.users.country-code-filter.whitelist}") List<String> whitelist) {
        Assert.notEmpty(whitelist, () -> "You must whitelist at least one country");
        this.countryCode = whitelist.get(0);
    }

    @Override
    public String countryCode(String ip) {
        return countryCode;
    }
}
