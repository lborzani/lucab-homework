package ch.ricardo.homework.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidSearchCriteria extends RuntimeException {

    public InvalidSearchCriteria(String message) {
        super(message);
    }

    public InvalidSearchCriteria(String message, Throwable cause) {
        super(message, cause);
    }
}
