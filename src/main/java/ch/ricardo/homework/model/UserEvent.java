package ch.ricardo.homework.model;

import ch.ricardo.homework.repositories.UserEntity;
import com.fasterxml.jackson.annotation.JsonCreator;

import java.time.OffsetDateTime;

// the password field is intentionally omitted
// as i don't see just now a reason for consumers to read the secret
public class UserEvent {

    private long id;
    private String firstName;
    private String email;
    private String address;
    private long timestamp;

    @JsonCreator
    public UserEvent(long id, String firstName, String email, String address) {
        this.id = id;
        this.firstName = firstName;
        this.email = email;
        this.address = address;
        this.timestamp = OffsetDateTime.now().toInstant().toEpochMilli();
    }

    public static UserEvent of(UserEntity entity) {
        // shortcut: it introduces probably unnecessary coupling that could be isolated in the entityListener
        return new UserEvent(entity.getId(), entity.getFirstName(), entity.getEmail(), entity.getAddress());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

}
