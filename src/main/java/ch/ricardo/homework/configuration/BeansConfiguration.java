package ch.ricardo.homework.configuration;

import ch.ricardo.homework.controllers.CountryCodeFilter;
import ch.ricardo.homework.services.GeolocationService;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Configuration
public class BeansConfiguration {

    private GeolocationService geolocationService;
    private List<String> countryCodesWhitelist;
    private String usersTopic;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @ConditionalOnProperty(value = "api.users.country-code-filter.enabled", matchIfMissing = true)
    @Bean
    public FilterRegistrationBean<CountryCodeFilter> countryCodeFilter() {
        Objects.requireNonNull(geolocationService,
                () -> "No instance of GeolocationService provided, or multiple instances are enabled an none is marked as @primary");
        FilterRegistrationBean<CountryCodeFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new CountryCodeFilter(geolocationService, countryCodesWhitelist));
        registrationBean.addUrlPatterns("/users");

        return registrationBean;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    // shortcut: topics should preexist, but normally they are not managed here, except for test environments
    @Bean
    public NewTopic usersTopic() {
        return TopicBuilder.name(usersTopic).partitions(1).replicas(1).build();
    }

    @Autowired
    public void setGeolocationService(ObjectProvider<GeolocationService> geolocationService) {
        this.geolocationService = geolocationService.getIfUnique();
    }

    @Autowired
    public void setCountryCodesWhitelist(@Value("${api.users.country-code-filter.whitelist}") List<String> countryCodesWhitelist) {
        this.countryCodesWhitelist = countryCodesWhitelist;
    }

    @Autowired
    public void setUsersTopic(@Value("${messaging.users-topic}") String usersTopic) {
        this.usersTopic = usersTopic;
    }
}
