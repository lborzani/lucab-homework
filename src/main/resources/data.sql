-- shortcut: using data.sql mechanism to avoid introducing liquibase or equivalent
DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(250) NOT NULL,
    email VARCHAR(250) NOT NULL,
    password VARCHAR(250) NOT NULL,
    address VARCHAR(500),
    UNIQUE KEY u_email (email)
);